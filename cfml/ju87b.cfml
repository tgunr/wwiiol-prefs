<?xml version='1.0'?>
<!DOCTYPE controlset SYSTEM "cfml.dtd">
<controlset version="1.1.0" language="english" keyboard="us">
	<control function="Gear handle">
		<key index="1"></key>
	</control>
	<control function="Deploy dive brakes">
		<joybutton stick="1" index="1">6</joybutton>
	</control>
	<control function="Stuka siren">
		<joybutton stick="1" index="1">8</joybutton>
	</control>
</controlset>
