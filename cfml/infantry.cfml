<?xml version='1.0'?>
<!DOCTYPE controlset SYSTEM "cfml.dtd">
<controlset version="1.1.0" language="english" keyboard="us">
	<control function="Gunsight Range Decrease">
		<key index="1"></key>
	</control>
	<control function="Gunsight Range Increase">
		<key index="1"></key>
	</control>
	<control function="Toggle Free Look">
		<key index="1"></key>
	</control>
	<control function="Sprint">
		<combo index="1">
			<key>left shift</key>
			<joybutton stick="3">1</joybutton>
		</combo>
	</control>
	<control function="Crouch">
		<joybutton stick="3" index="1">5</joybutton>
	</control>
	<control function="Prone">
		<joybutton stick="3" index="1">10</joybutton>
	</control>
	<control function="Forward">
		<joybutton stick="3" index="1">3</joybutton>
	</control>
	<control function="Backward">
		<joybutton stick="3" index="1">8</joybutton>
	</control>
	<control function="Strafe right">
		<joybutton stick="3" index="1">9</joybutton>
	</control>
	<control function="Strafe left">
		<joybutton stick="3" index="1">7</joybutton>
	</control>
	<control function="Trooper Look Left">
		<joybutton stick="3" index="1">2</joybutton>
	</control>
	<control function="Trooper Look Right">
		<joybutton stick="3" index="1">4</joybutton>
	</control>
	<control function="Reload">
		<key index="1"></key>
	</control>
	<control function="Gunsight\Alt Deploy">
		<key>x</key>
		<mousebutton index="1">2</mousebutton>
	</control>
	<control function="Select Next Weapon">
		<mousebutton index="1">3</mousebutton>
	</control>
	<control function="Select Previous Weapon">
		<key index="1"></key>
	</control>
	<control function="Select Weapon 1">
		<key index="1"></key>
	</control>
	<control function="Select Weapon 10">
		<key index="1"></key>
	</control>
</controlset>
