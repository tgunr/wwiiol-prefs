<?xml version='1.0'?>
<!DOCTYPE controlset SYSTEM "cfml.dtd">
<controlset version="1.1.0" language="english" keyboard="us">
	<control function="Shift up">
		<joybutton stick="1" index="1">19</joybutton>
	</control>
	<control function="Shift down">
		<joybutton stick="1" index="1">21</joybutton>
	</control>
	<control function="Instrument view">
		<joybutton stick="1" index="1">3</joybutton>
	</control>
	<control function="Gunsight view">
		<joybutton stick="1" index="1">2</joybutton>
	</control>
	<control function="Use primary weapon">
		<joybutton stick="1">1</joybutton>
		<key index="1"></key>
	</control>
	<control function="Use secondary weapon">
		<joybutton stick="1">8</joybutton>
		<key index="1"></key>
	</control>
	<control function="Gunsight Range Decrease">
		<joybutton stick="1" index="1">17</joybutton>
	</control>
	<control function="Gunsight Range Increase">
		<joybutton stick="1" index="1">15</joybutton>
	</control>
	<control function="Position 1">
		<joybutton stick="1" index="1">15</joybutton>
	</control>
	<control function="Position 2">
		<joybutton stick="1" index="1">16</joybutton>
	</control>
	<control function="Position 3">
		<joybutton stick="1" index="1">17</joybutton>
	</control>
	<control function="Position 4">
		<key index="1"></key>
	</control>
	<control function="Position 5">
		<key index="1"></key>
	</control>
	<control function="Position 6">
		<joybutton stick="1" index="1">18</joybutton>
	</control>
	<control function="Position 7">
		<combo index="1">
			<joybutton stick="1">POV1w</joybutton>
			<joybutton stick="2">POV1w</joybutton>
		</combo>
	</control>
	<control function="Position 8">
		<combo index="1">
			<joybutton stick="1">POV1e</joybutton>
			<joybutton stick="2">POV1e</joybutton>
		</combo>
	</control>
	<control function="Position 9">
		<joybutton stick="3" index="1">9</joybutton>
	</control>
	<control function="Position 10">
		<joybutton stick="3" index="1">10</joybutton>
	</control>
	<control function="Gas">
		<joyaxis stick="1">rx</joyaxis>
	</control>
	<control function="Steering">
		<joyaxis stick="1">rz</joyaxis>
	</control>
	<control function="Turret traverse">
		<joyaxis stick="1">x</joyaxis>
		<keyabsolute value="0.00" onrelease="50.00">
			<key>d</key>
		</keyabsolute>
	</control>
	<control function="Turret elevate">
		<joyaxis stick="1" invert="1">y</joyaxis>
	</control>
</controlset>
