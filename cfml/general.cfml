<?xml version='1.0'?>
<!DOCTYPE controlset SYSTEM "cfml.dtd">
<controlset version="1.1.0" language="english" keyboard="us">
	<control function="Map Zoom In">
		<key>=</key>
		<joybutton stick="1" index="1">25</joybutton>
	</control>
	<control function="Map Zoom Out">
		<key>-</key>
		<joybutton stick="1" index="1">23</joybutton>
	</control>
	<control function="Show in game map">
		<joybutton stick="1" index="1">24</joybutton>
	</control>
	<control function="Show Game Info">
		<key>f12</key>
	</control>
	<control function="Cycle Icon Mode">
		<joybutton stick="1" index="1">26</joybutton>
	</control>
	<control function="Change Chat height">
		<key>tab</key>
	</control>
</controlset>
