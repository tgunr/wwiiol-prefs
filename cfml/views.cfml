<?xml version='1.0'?>
<!DOCTYPE controlset SYSTEM "cfml.dtd">
<controlset version="1.1.0" language="english" keyboard="us">
	<control function="Look Forward">
		<key></key>
	</control>
	<control function="Look Back">
		<key index="1"></key>
	</control>
	<control function="Look Up">
		<key>space</key>
		<joybutton stick="1" index="1">7</joybutton>
	</control>
	<control function="Look Down">
		<key></key>
		<key index="1"></key>
	</control>
	<control function="Look Forward + Right">
		<key></key>
	</control>
	<control function="Look Forward + Left">
		<key></key>
	</control>
	<control function="Look Right">
		<key></key>
	</control>
	<control function="Look Left">
		<key></key>
	</control>
	<control function="Look Back + Right">
		<key></key>
	</control>
	<control function="Look Back + Left">
		<key></key>
	</control>
	<control function="Look Forward + Up">
		<key></key>
		<joybutton stick="1" index="1">POV1n</joybutton>
	</control>
	<control function="Look Forward + Right + Up">
		<joybutton stick="1">20</joybutton>
		<key index="1"></key>
	</control>
	<control function="Look Forward + Left + Up">
		<joybutton stick="1">22</joybutton>
		<key index="1"></key>
	</control>
	<control function="Look Right + Up">
		<key></key>
		<key index="1"></key>
	</control>
	<control function="Look Left + Up">
		<key></key>
		<key index="1"></key>
	</control>
	<control function="Look Back + Right + Up">
		<joybutton stick="1">19</joybutton>
		<key index="1"></key>
	</control>
	<control function="Look Back + Left + Up">
		<joybutton stick="1">21</joybutton>
		<key index="1"></key>
	</control>
	<control function="Toggle Free Look">
		<key index="1"></key>
	</control>
	<control function="Mouse Look Y">
		<mouseaxis>y</mouseaxis>
	</control>
</controlset>
