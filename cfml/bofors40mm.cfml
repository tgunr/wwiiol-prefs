<?xml version='1.0'?>
<!DOCTYPE controlset SYSTEM "cfml.dtd">
<controlset version="1.1.0" language="english" keyboard="us">
	<control function="Turret traverse">
		<joyaxis stick="1">x</joyaxis>
	</control>
	<control function="Turret elevate">
		<joyaxis stick="1">y</joyaxis>
	</control>
</controlset>
